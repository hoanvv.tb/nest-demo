import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PostDTO } from './dto/post.dto';
import { Post } from './schemas/post.schema';


// service provide method to CRUD

@Injectable()
export class PostService {
    constructor(@InjectModel('Post') private PostModel: Model<Post>) {}

    // create
    async create(PostDTO: PostDTO): Promise<Post> {
        const post = new this.PostModel(PostDTO);
        return post.save();
    }

    // get all
    async findAll(): Promise<Post[]> {
        return await this.PostModel.find().exec();
    }

    // get by id
    async findById(id: string): Promise<Post> {
        const post = await this.PostModel.findById(id).exec();
        return post;
    }

    // get one
    async find(req): Promise<Post[]> {
        return await this.PostModel.find(req).exec();
    }

    // update
    async update(id: string, PostDTO: PostDTO): Promise<Post> {
        return await this.PostModel.findByIdAndUpdate(id, PostDTO, {new: true});
    }

    // delete
    async delete(id: string): Promise<Post> {
        return await this.PostModel.findByIdAndRemove(id);
    }
}
