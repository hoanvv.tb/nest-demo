import { Schema, SchemaFactory, Prop } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { User } from "src/user/schemas/user.schema";

export type PostDocument = Post & mongoose.Document;

export class Post {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'User'})
    user: User;

    @Prop({required: true})
    fullname: string;

    @Prop({required: true})
    content: string;

    @Prop({required: true})
    imageURL: string;
    
    @Prop({required: true})
    videoURL: string;

    @Prop({required: true})
    webURL: string;
}
export const PostSchema = SchemaFactory.createForClass(Post);