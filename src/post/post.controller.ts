import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Post, Put, Query, Res } from '@nestjs/common';
import { PostDTO } from './dto/post.dto';
import { PostService } from './post.service';

@Controller('post')
export class PostController {
    constructor(private readonly PostService: PostService) {}

    // create post
    @Post('/create')
    async createPost(@Res() res, @Body() PostDTO: PostDTO) {
        console.log({PostDTO, res});
        
        const posts = await this.PostService.create(PostDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Post has been create successfully',
            posts
        })
    }

    // get all posts
    @Get('get-all-posts')
    async getAllPosts(@Res() res) {
        const posts = await this.PostService.findAll();
        return res.status(HttpStatus.OK).json(posts)
    }

    // get post by id
    @Get('get-post')
    async getPostById(@Res() res, @Query('id') id: string) {
        const post = await this.PostService.findById(id);
        if(!post) throw new NotFoundException('Not found');
        return res.status(HttpStatus.OK).json(post)
    }

    // update post by id
    @Put('/update-post')
    async updatePostById(@Res() res, @Query('id') id: string, @Body() PostDTO: PostDTO) {
        const post = await this.PostService.update(id, PostDTO);
        if(!post) throw new NotFoundException('Cannot update');
        return res.status(HttpStatus.OK).json({
            message: 'Update post successfully',
            post
        })
    }

    // delete post by id
    @Delete('remove-post')
    async removePostById(@Res() res, @Query('id') id: string) {
        const post = await this.PostService.delete(id);
        if(!post) throw new NotFoundException('Cannot delete');
        return res.status(HttpStatus.OK).json({
            message: 'Delete post successfully',
            post
        })
    }
}
