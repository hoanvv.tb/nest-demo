import { User } from "src/user/interfaces/user.interface";

export interface Post {
    readonly user: User;
    readonly content: string;
    readonly imageURL: string;
    readonly videoURL: string;
    readonly webURL: string;
}