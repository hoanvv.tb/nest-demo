import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/user/interfaces/user.interface";

export class PostDTO {
    @ApiProperty({required: true})
    readonly user?: User;

    @ApiProperty({required: true})
    readonly content: string;

    @ApiProperty()
    readonly imageURL: string;

    @ApiProperty()
    readonly videoURL: string;

    @ApiProperty()
    readonly webURL: string;

}