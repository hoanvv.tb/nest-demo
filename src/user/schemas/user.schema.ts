// import * as mongoose from 'mongoose';
// const Schema = mongoose.Schema;
// export const UserSchema = new Schema({
//     name: String,
//     age: Number,
//     address: String,
// });
import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;
@Schema()
export class User {
    @Prop({required: true})
    username: string;

    @Prop({required: true})
    fullname: string;

    @Prop({required: true})
    dob: Date;

    @Prop({required: true})
    gender: string;

    @Prop()
    address: string;
}

export const UserSchema = SchemaFactory.createForClass(User);