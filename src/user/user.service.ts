import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/user.dto';
@Injectable()
export class UserService {
    constructor(@InjectModel('User') private UserModel: Model<User>) { }
    async create(CreateUserDTO: CreateUserDTO): Promise<any> {
        const createdUser = new this.UserModel(CreateUserDTO);
        return createdUser.save();
    }
    async findAll(): Promise<User[]> {
        return await this.UserModel.find().exec();
    }
    async findById(id: string): Promise<User> {
        const customer = await this.UserModel.findById(id).exec();
        return customer;
    }
    async find(req): Promise<User[]> {
        return await this.UserModel.find(req).exec();
    }
    async update(id: string, CreateUserDTO: CreateUserDTO): Promise<User> {
        return await this.UserModel.findByIdAndUpdate(id, CreateUserDTO, { new: true });
    }
    async delete(id: string): Promise<User> {
        return await this.UserModel.findByIdAndRemove(id);
    }
}