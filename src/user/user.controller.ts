import { Controller, Res, Query, Get, HttpStatus, Post, Body, Param, NotFoundException, Put, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiQuery } from '@nestjs/swagger';
import { CreateUserDTO } from './dto/user.dto';
@Controller('user')
export class UserController {
    constructor(private readonly UserService: UserService) { }
    @Post('/create')
    async addCustomer(@Res() res, @Body() CreateUserDTO: CreateUserDTO) {
        const users = await this.UserService.create(CreateUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "User has been created successfully",
            users
        })
    }
    @Get('all')
    async findAll(@Res() res) {
        const users = await this.UserService.findAll();
        return res.status(HttpStatus.OK).json(users);
    }
    @Get('id')
    async findById(@Res() res, @Query('id') id: string) {
        const user = await this.UserService.findById(id);
        if (!user) throw new NotFoundException('Id does not exist!');
        return res.status(HttpStatus.OK).json(user);
    }
    @Put('/update')
    async update(@Res() res, @Query('id') id: string, @Body() CreateUserDTO: CreateUserDTO) {
        const user = await this.UserService.update(id, CreateUserDTO);
        if (!user) throw new NotFoundException('Id does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'User has been successfully updated',
            user
        });
    }
    @Delete('/delete')
    async delete(@Res() res, @Query('id') id: string) {
        const user = await this.UserService.delete(id);
        if (!user) throw new NotFoundException('Post does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'User has been deleted',
            user
        })
    }
}