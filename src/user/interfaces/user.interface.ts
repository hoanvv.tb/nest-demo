import { Document } from 'mongoose';
export interface User extends Document {
    readonly username: string;
    readonly fullname: string;
    readonly address: string;
    readonly dob: Date;
    readonly gender: string;
}