import { ApiProperty } from '@nestjs/swagger';
export class CreateUserDTO {
    @ApiProperty({required: true})
    readonly username: string;

    @ApiProperty({required: true})
    readonly fullname: string;

    @ApiProperty({required: true})
    readonly dob: Date;

    @ApiProperty({required: true})
    readonly gender: string;
    
    @ApiProperty()
    readonly address: string;
}