import { config } from 'dotenv';
config();

export const databaseConfig = {
    db_connection: `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
}