import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { databaseConfig } from './config/database';
import { PostModule } from './post/post.module';

@Module({
  imports: [
    UserModule,
    PostModule,
    MongooseModule.forRoot(databaseConfig.db_connection)
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
